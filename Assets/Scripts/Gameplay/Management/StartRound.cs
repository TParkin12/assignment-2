﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;





public class StartRound : MonoBehaviour
{
    public enum RoundState
    {
        Transition,
        Progress,
        RoundEnd,
        GAMEOVER,
        StartRound
    };

    public RoundState RoundStatus;

    private void Start()
    {
        RoundStatus = RoundState.StartRound;
        FindObjectOfType<AudioManager>().Play("GameBGM");
         
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player") && (RoundStatus == RoundState.StartRound  || RoundStatus == RoundState.RoundEnd))
        {
            FindObjectOfType<Spawner>().StartSpawning = true;
            Time.timeScale = 1;
            FindObjectOfType<ModifierManagment>().enabled = true;
            RoundStatus = RoundState.Progress;
        }
    }

    private void Update()
    {
        switch(RoundStatus)
        {
            case RoundState.Progress:


                 


                if (FindObjectOfType<HudManagment>().EnemiesLeft == 0)
                {
                    StartCoroutine(Delay());
                    FindObjectOfType<AudioManager>().Play("CombatMusic");
                    FindObjectOfType<AudioManager>().Stop("GameBGM");
                }

                break;

            case RoundState.RoundEnd:

                

                break;

            case RoundState.Transition:


                RoundStatus = RoundState.RoundEnd;
                FindObjectOfType<AudioManager>().Stop("CombatMusic");
                FindObjectOfType<AudioManager>().Play("Transition");

                StartCoroutine(Transition());


                

                
                break;

            case RoundState.GAMEOVER:


                break;

            default:

               //FindObjectOfType<AudioManager>().Play("GameBGM");
                break;
        }

        if(GameManager.Instance.gameState == GameManager.GameState.GAME_OVER)
        {
            RoundStatus = RoundState.GAMEOVER;
        }
    }
      

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(2);

        if (FindObjectOfType<HudManagment>().EnemiesLeft == 0)
        {
            FindObjectOfType<Spawner>().StartSpawning = false;

            RoundStatus = RoundState.Transition;
        }
    }


    IEnumerator Transition()
    {
        

        

        yield return new WaitForSeconds(5);

        FindObjectOfType<AudioManager>().Stop("Transition");

        GameObject.FindGameObjectWithTag("Player").transform.position = GameObject.Find("SpawnPoint").transform.position;

        
        FindObjectOfType<AudioManager>().Play("GameBGM");

        

    }
}
