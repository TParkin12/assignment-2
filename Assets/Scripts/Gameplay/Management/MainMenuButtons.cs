﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MainMenuButtons : MonoBehaviour
{

    

    [SerializeField]private Button StartGameBtn;
    [SerializeField]private Button RulesBtn;
    [SerializeField]private Button QuitBtn;


    [SerializeField] private Button RestartBtn;
    [SerializeField] private Button MenuBtn;
    [SerializeField] private Button LeaderboardsBTN;
    [SerializeField] private Button QuitGBtn;
     
    
    
    [SerializeField] private Button ReturnBtn;


    private Scene CurrentScene;


   

    // Start is called before the first frame update
    void FixedUpdate()
    {
        CurrentScene = SceneManager.GetActiveScene();

        switch(CurrentScene.name)
        {
            case "Start":
                StartGameBtn = GameObject.Find("StartButton").GetComponent<Button>();
                QuitBtn = GameObject.Find("QuitButton").GetComponent<Button>();
                RulesBtn = GameObject.Find("RulesButton").GetComponent<Button>();
                LeaderboardsBTN = GameObject.Find("LeaderboardsBtn").GetComponent<Button>();

                StartGameBtn.onClick.AddListener(() =>
                {
                    GameManager.Instance.PlayGame();
                });

                RulesBtn.onClick.AddListener(() =>
                {
                    gameObject.GetComponent<GameManager>().DisplayRules();
                });

                QuitBtn.onClick.AddListener(() =>
                {
                    GameManager.Instance.QuitGame();
                });

                LeaderboardsBTN.onClick.AddListener(() =>
                {
                    GameManager.Instance.LoadBoard();
                });


                break;

            case "Rules":
                
                    ReturnBtn = GameObject.Find("ReturnToMenuBtn").GetComponent<Button>();



                    ReturnBtn.onClick.AddListener(gameObject.GetComponent<GameManager>().StartGame);

                   
                


                break;

            
                

            case "Leaderboard":
                ReturnBtn = GameObject.Find("ReturnToMenuBtn").GetComponent<Button>();

                ReturnBtn.onClick.AddListener(gameObject.GetComponent<GameManager>().StartGame);
                break;

            case "Game":
                QuitGBtn = GameObject.Find("QuitButton").GetComponent<Button>();
                RestartBtn = GameObject.Find("RestartButton").GetComponent<Button>();
                MenuBtn = GameObject.Find("MainMenuButton").GetComponent<Button>();
                LeaderboardsBTN = GameObject.Find("LeaderboardsBtn").GetComponent<Button>();




                RestartBtn.onClick.AddListener(() =>
                {
                    GameManager.Instance.PlayGame();
                });

                MenuBtn.onClick.AddListener(() =>
                {
                    GameManager.Instance.StartGame();
                });

                QuitGBtn.onClick.AddListener(() =>
                {
                    GameManager.Instance.QuitGame();
                });


                LeaderboardsBTN.onClick.AddListener(() =>
                {
                    GameManager.Instance.LoadBoard();
                });


                break;


            default:
                return;
                
        }

       
        
        





    }

    void returnToMenu()
    {
        SceneManager.LoadScene("Start");
    }
}
