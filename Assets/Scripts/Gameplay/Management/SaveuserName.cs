﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SaveuserName : MonoBehaviour
{
    public void EnterName(string name)
    {
        name = GameObject.Find("NameInputText").GetComponent<Text>().text;
        SaveData.Current.Name = name;
        SaveData.Current.HighScore = (HudManagment.ScoreCount);
        Debug.Log(SaveData.Current.HighScore);
        GameManager.Save();
        
    }
}
