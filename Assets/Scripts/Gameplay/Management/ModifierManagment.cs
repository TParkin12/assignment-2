﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ModifierManagment : MonoBehaviour
{
    public enum ModifierName
    {
        
        DoubleSpawn,
        Damage,
        Health,
        TimeSlip,
        Turret,
        Drone,
        None
    };


    public GameObject GnomeSpawner;
    public GameObject TurretSpawner;
    public GameObject DroneSpawner;
    public ModifierName Modifier;

    private Text ModiferDescription;

    private float BaseDamage;
    private float BaseHealth;
    private float BaseMovementSpeed;
    private float BaseJumpHeight;

   
    public bool ModifiersActive;

    private void Awake()
    {
        
        BaseHealth = FindObjectOfType<PlayerStats>().HealthPoints;


        ModiferDescription = GameObject.Find("ActiveModiferTXT").GetComponent<Text>();

        TurretSpawner.SetActive(false);
        GnomeSpawner.SetActive(true);
        DroneSpawner.SetActive(false);

        ModifiersActive = false;
    }


    private void Start()
    {



        Modifier = ModifierName.Drone;
            
            
            switch (Modifier)
            {
                case ModifierName.None:



                    FindObjectOfType<PlayerStats>().HealthPoints = BaseHealth;

                    ModiferDescription.text = "None";



                    break;

                case ModifierName.Damage:

                FindObjectOfType<PlayerStats>().doubleDamage = true;
                    ModiferDescription.text = "Increased Enemy Damage";

                    

                    break;

                case ModifierName.Health:

                    FindObjectOfType<PlayerStats>().HealthPoints = BaseHealth * 0.5f;

                    ModiferDescription.text = "Half Health";

                    break;

                case ModifierName.DoubleSpawn:

                    ModiferDescription.text = "Double the Enemies";

                FindObjectOfType<Spawner>().SpawnIncrease(FindObjectOfType<Spawner>().Max_Spawn * 2);

                    break;

                case ModifierName.TimeSlip:

                    ModiferDescription.text = "Game speed increased";

                Time.timeScale = 1.3f;
                break;

            case ModifierName.Turret:
                TurretSpawner.SetActive(true);
                GnomeSpawner.SetActive(false);

                ModiferDescription.text = "Turret Round";
                    break;

            case ModifierName.Drone:
                DroneSpawner.SetActive(true);
                GnomeSpawner.SetActive(false);

                ModiferDescription.text = "Drone Round";
                break;
        }
        
         
    }
}