﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ShopMenu : MonoBehaviour
{
    private enum State
    {
      Open,
      Closed
    };

    private enum WeaponType
    {
      Rocket,
      Single,
      Auto
    };

    private State ShopStatus;
    private WeaponType Weapon;

    public GameObject RocketLauncherGO;
    public GameObject SingleGO;
    public GameObject AutoGO;

    public Text DamageText;
    public Text AmmoTXT;

    private Animator anim;
    private Button RocketLauncher;
    private Button SingleShotgun;
    private Button AutoShotgun;
    private Button Health;
    private Button Damage;

    private float DamageCost;
    private float HealthCost;
    private float HealthTemp;
    [SerializeField]private float CashSpent;


    private void Start()
    {
        RocketLauncher = GameObject.Find("RocketBtn").GetComponent<Button>();
        AutoShotgun = GameObject.Find("AutoShotgunBtn").GetComponent<Button>();
        SingleShotgun = GameObject.Find("SingleShotgunBtn").GetComponent<Button>();
        Health = GameObject.Find("HealthBtn").GetComponent<Button>();
        Damage = GameObject.Find("DamageBtn").GetComponent<Button>();

        DamageText.text = "Damage: " + SingleGO.GetComponent<Gun>().HitDamage;

        HealthTemp = FindObjectOfType<PlayerStats>().HealthPoints ;

        anim = GameObject.Find("Doors").GetComponent<Animator>();

        Weapon = WeaponType.Single; 
        DamageCost = 10000;
        HealthCost = 10000;

        CashSpent = 0;

        RocketLauncher.onClick.AddListener(RocketLauncherButtonCheck);
        AutoShotgun.onClick.AddListener(AutoShotgunButtonCheck);
        SingleShotgun.onClick.AddListener(SingleShotgunButtonCheck);
        Health.onClick.AddListener(HealthButtonCheck);
        Damage.onClick.AddListener(DamageButtonCheck);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (FindObjectOfType<StartRound>().RoundStatus == StartRound.RoundState.RoundEnd && other.CompareTag("Player"))
        {
            int random = Random.Range(0, 10);
            Debug.Log(random);
            if(FindObjectOfType<ModifierManagment>().Modifier == ModifierManagment.ModifierName.DoubleSpawn)
            {
                FindObjectOfType<Spawner>().SpawnIncrease(FindObjectOfType<Spawner>().Max_Spawn / 2);
            }
            FindObjectOfType<Spawner>().SpawnIncrease(random);
            FindObjectOfType<Spawner>().StartSpawning = false;
            FindObjectOfType<ModifierManagment>().enabled = false;
            FindObjectOfType<PlayerStats>().doubleDamage = false;
        }

        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && Input.GetKeyDown(KeyCode.B))
        {
            anim.SetBool("OpenDoor", true);

            FindObjectOfType<AudioManager>().Play("Door");
        }

        switch (ShopStatus)
        {
            case State.Open:
                if (other.CompareTag("Player") && Input.GetKeyDown(KeyCode.E))
                {
                    GameObject.Find("ShopMenu").GetComponent<Canvas>().enabled = false;
                    ShopStatus = State.Closed;
                    GameManager.Instance.EnableInput(true);
                    Cursor.lockState = CursorLockMode.Locked;

                }

                

                

                break;
            case State.Closed:
                if (other.CompareTag("Player") && Input.GetKeyDown(KeyCode.E))
                {
                    GameObject.Find("ShopMenu").GetComponent<Canvas>().enabled = true;
                    ShopStatus = State.Open;
                    GameManager.Instance.EnableInput(false);
                    Cursor.lockState = CursorLockMode.None;

                   


                }
                break;
        }

        
        
         


    }

    private void OnTriggerExit(Collider other)
    {
        anim.SetBool("OpenDoor", false);

        FindObjectOfType<AudioManager>().Stop("Door");
    }

    private void Update()
    {
        WeaponHUD();
    }


    public void WeaponHUD()
    {
        switch(Weapon)
        {
            case WeaponType.Rocket:
                AmmoTXT.text = "Ammo: " + RocketLauncherGO.GetComponent<RocketLauncher>().Ammo_Stock;
                break;

            default:
                AmmoTXT.text = "Ammo: " + "Infinite";
                break;
        }
        
    }

    public void RocketLauncherButtonCheck()
    {
       
        
            if (HudManagment.CashCount >= 8000)
            {
                CashSpent = 800;
                Weapon = WeaponType.Rocket;
                ShopButtons();
                FindObjectOfType<HudManagment>().SpendCash(CashSpent);
            }
            else
            {
               // RocketLauncher.interactable = false;
            }

        
    }
    public void AutoShotgunButtonCheck()
    {
         

            if (HudManagment.CashCount >= 4000)
            {
                CashSpent = 400;
                Weapon = WeaponType.Auto;
                ShopButtons();
                FindObjectOfType<HudManagment>().SpendCash(CashSpent);
            }
            else
            {
                //AutoShotgun.interactable = false;
            }

        
    }
    public void SingleShotgunButtonCheck()
    {
        
            if (HudManagment.CashCount >= 2000)
            {
                CashSpent = 200;
                Weapon = WeaponType.Single;
                ShopButtons();
                FindObjectOfType<HudManagment>().SpendCash(CashSpent);
            }
            else
            {
                //SingleShotgun.interactable = false;
            }

        
    }
  
    public void HealthButtonCheck()
    {


        if (HudManagment.CashCount >= HealthCost)
        {
            CashSpent = HealthCost;
            ShopButtons();
             
                HealthTemp *= 2;
                FindObjectOfType<PlayerStats>().HealthPoints = HealthTemp;
            

            
            
            FindObjectOfType<HudManagment>().SpendCash(CashSpent);
            HealthCost *= 2;
        }
        else
        {
            //Health.interactable = false;
        }


    }

    public void DamageButtonCheck()
    {


        if (HudManagment.CashCount >= DamageCost)
        {
            CashSpent = DamageCost;
            FindObjectOfType<HudManagment>().SpendCash(CashSpent);
            DamageCost *= 2;

            switch (Weapon)
            {
                case WeaponType.Rocket:
                    RocketLauncherGO.GetComponent<RocketLauncher>().Ex_Damage += 20;
                    break;

                case WeaponType.Auto:
                    AutoGO.GetComponent<AutoGun>().HitDamage += 20;

                    break;

                case WeaponType.Single:
                    SingleGO.GetComponent<Gun>().HitDamage += 20;
                    break;
            }

            ShopButtons();

        }
        else
        {
           // Damage.interactable = false;
        }


    }


    private void ShopButtons()
    {
        switch (Weapon)
        {
            case WeaponType.Rocket:
                if (RocketLauncherGO.activeInHierarchy == false)
                {
                    RocketLauncherGO.SetActive(true);
                    AutoGO.SetActive(false);
                    SingleGO.SetActive(false);
                }
                else
                {
                    RocketLauncherGO.GetComponent<RocketLauncher>().Ammo_Stock = 40;
                    
                }
                    DamageText.text = "Damage:" + RocketLauncherGO.GetComponent<RocketLauncher>().Ex_Damage;
                break;

            case WeaponType.Auto:
                if (AutoGO.activeInHierarchy == false)
                {
                    AutoGO.SetActive(true);
                    RocketLauncherGO.SetActive(false);
                    SingleGO.SetActive(false);

                    
                }
                DamageText.text = "Damage:" + AutoGO.GetComponent<AutoGun>().HitDamage;
                break;

            case WeaponType.Single:
                if (SingleGO.activeInHierarchy == false)
                {
                    SingleGO.SetActive(true);
                    AutoGO.SetActive(false);
                    RocketLauncherGO.SetActive(false);

                    
                }
                DamageText.text = "Damage:" + SingleGO.GetComponent<Gun>().HitDamage;

                break;
        }
    }
}
