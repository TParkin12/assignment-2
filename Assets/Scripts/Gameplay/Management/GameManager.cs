﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;



public class GameManager : MonoBehaviour {

    public enum  GameState { START, IN_GAME, GAME_OVER, DISPLAY_RULES, BOARD };
    
    public GameState gameState;
    [SerializeField] private GameState startState = GameState.START; // Exists to enable individual level testing


    [SerializeField] private GameObject gameOverPrefab;



    static private GameManager instance = null;

    // Lets other scripts find the instane of the game manager
    public static GameManager Instance
    {
        get
        {
            return instance;
        }

         
    }

    // Ensure there is only one instance of this object in the game
    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
          

        instance = this;
    }

    void Start()
    {
        gameState = startState;

        if(startState == GameState.START)
        {
            FindObjectOfType<AudioManager>().Play("MenuMusic");
            Load();
        }
        

        

    }

    void OnChangeState(GameState newState)
    {
        if (gameState != newState)
        {
            switch (newState)
            {
                case GameState.START:

                    Time.timeScale = 1;
                    SceneManager.LoadScene("Start");

                    FindObjectOfType<AudioManager>().Stop("GameBGM");
                    FindObjectOfType<AudioManager>().Play("MenuMusic");


                    //GameObject.Find("MainCanvas").GetComponent<Canvas>().enabled = true;
                    //GameObject.Find("RulesCanvas").GetComponent<Canvas>().enabled = false;

                    Cursor.lockState = CursorLockMode.Confined; // Lock the cursor to the game screen
                    Cursor.visible = true;

                    
                   

                    break;
                case GameState.IN_GAME:

                    gameOverPrefab.GetComponent<Canvas>().enabled = false; 

                    Time.timeScale = 1; // Set timescale to be a normal rate 
                    SceneManager.LoadScene("Game"); // Load the 'Game' scene  

                    FindObjectOfType<AudioManager>().Stop("MenuMusic");
                    FindObjectOfType<AudioManager>().Play("GameBGM");

                    Cursor.lockState = CursorLockMode.Locked; // Lock the cursor to the game screen
                    Cursor.visible = false;


                    
                    break;


                case GameState.GAME_OVER:

                    Cursor.lockState = CursorLockMode.Confined; // unlock the cursor for the menu
                    Cursor.visible = true;

                    EnableInput(false); // disable character controls

                    Time.timeScale = 0; // Pause the game by setting timescale to 0 to stop AI behaviour
                    gameOverPrefab.GetComponent<Canvas>().enabled = true; // Instantiate the GameOver menu prefab

                    FindObjectOfType<AudioManager>().StopAllAudio();

                    break;


                case GameState.DISPLAY_RULES:

                    Time.timeScale = 1;
                    SceneManager.LoadScene("Rules");
                   // GameObject.Find("MainCanvas").GetComponent<Canvas>().enabled = false;
                   // GameObject.Find("RulesCanvas").GetComponent<Canvas>().enabled = true;
                    Cursor.lockState = CursorLockMode.Confined; // Lock the cursor to the game screen
                    Cursor.visible = true;

                    

                    break;

                case GameState.BOARD:

                    Time.timeScale = 1;
                    SceneManager.LoadScene("Leaderboard");
                    Cursor.lockState = CursorLockMode.Confined; // Lock the cursor to the game screen
                    Cursor.visible = true;
                    break;

            }

            

            gameState = newState;
        }
    }

    public void EnableInput(bool input)
    {
        // Find the player object
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<FirstPersonController>().enabled = input;
       if(player.GetComponentInChildren<Gun>() != null)
        {
            player.GetComponentInChildren<Gun>().enabled = input;
           
            
        }
        else if (player.GetComponentInChildren<AutoGun>() != null)
        {
             
            player.GetComponentInChildren<AutoGun>().enabled = input;
            
        }
        else if (player.GetComponentInChildren<RocketLauncher>() != null)
        {
             
            
            player.GetComponentInChildren<RocketLauncher>().enabled = input;
        }

    }

    public void StartGame()
    {
        OnChangeState(GameState.START);
    }


    public void DisplayRules()
    {
        OnChangeState(GameState.DISPLAY_RULES);
         
    }


    public void PlayGame()
    {
        OnChangeState(GameState.IN_GAME);
         
        
    }

    public void GameOver()
    {
        OnChangeState(GameState.GAME_OVER);
         
    }

    public void QuitGame()
    {
        Application.Quit();
       
    }

    public void LoadBoard()
    {
        OnChangeState(GameState.BOARD);

    }




    public static void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/SavedGame.gd");

        bf.Serialize(file, SaveData.Current);
        file.Close();
    }

    public static void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/SaveGame.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/SaveGame.gd", FileMode.Open);
            SaveData.Current = (SaveData)bf.Deserialize(file);
            file.Close();
        }
        else
        {
            SaveData.Current = new SaveData();
        }
    }


}
