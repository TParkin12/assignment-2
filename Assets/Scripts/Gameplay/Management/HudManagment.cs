﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HudManagment : MonoBehaviour
{
    private Text Score;
    public static float ScoreCount;

    private Text Cash;
    public static float CashCount;

    private Text HealthTXT;
    private float HP;

    public Image HealthBar;
    public float MaxHP;

    private Text EnemyCountText;
    private GameObject[] Enemies;
    public float EnemiesLeft;


    void Start()
    {

        if (SceneManager.GetActiveScene().name == "Game")
        {
            Score = GameObject.Find("ScoreText").GetComponent<Text>();
            ScoreCount = 0;

            Cash = GameObject.Find("CashText").GetComponent<Text>();
            CashCount = 0;

            HealthTXT = GameObject.Find("HealthText").GetComponent<Text>();
            HP = FindObjectOfType<PlayerStats>().HealthPoints;

            HealthBar = GameObject.Find("HealthBar").GetComponent<Image>();
            MaxHP = HP;

            EnemyCountText = GameObject.Find("AmountleftTXT").GetComponent<Text>();
            EnemyCountText.text = EnemiesLeft.ToString();
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        if(SceneManager.GetActiveScene().name == "Game")
        {
            if (Score != null)
            {
                Score.text = ("Score: " + ScoreCount);
            }

            if (Cash != null)
            {
                Cash.text = ("Score: " + CashCount);
            }

            if (HealthTXT != null)
            {
                HealthTXT.text = ("Health: " + HP);
            }

            
            HP = FindObjectOfType<PlayerStats>().HealthPoints;

            if (HealthBar != null)
            {
                HealthBar.fillAmount = HP / MaxHP;
            }

            if (EnemyCountText != null)
            {
                

               
                if (FindObjectOfType<StartRound>().RoundStatus == StartRound.RoundState.Progress)  
                {
                    Enemies = GameObject.FindGameObjectsWithTag("Enemy");

                    EnemiesLeft = Enemies.Length;

                    EnemyCountText.text = EnemiesLeft.ToString();
                }
            }



            if (GameManager.Instance.gameState == GameManager.GameState.GAME_OVER)
            {
                DisplayScore();
            }
        }

    }



    public void AddToScore(float ScoreADD)
    {
        ScoreCount += ScoreADD;
        CashCount += ScoreADD;

    }

    public void SpendCash(float Cost)
    {
        CashCount -= Cost;
    }


    private void DisplayScore()
    {
        GameObject.Find("Title").GetComponent<Text>().text = "Final Score: " + ScoreCount;
    }

}
