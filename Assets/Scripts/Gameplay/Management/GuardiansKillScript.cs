﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardiansKillScript : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
       
        if(other.CompareTag("Player"))
        {
            Destroy(gameObject, 3);
            FindObjectOfType<DeathAnimation>().IsKilled = (true);

        }
       
    }


  
}
