﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Leaderboard : MonoBehaviour
{
    [SerializeField] Transform Template;
    [SerializeField] Transform Container;
    [SerializeField] GameObject TemplateGO;

    private float BaseHeight;

    private List<SaveData> HighScores;

    

    private void Awake()
    {

        BaseHeight = 25;

        TemplateGO = GameObject.Find("ScoreTemplate");


        HighScores = new List<SaveData>()
        {
            new SaveData{HighScore = 1000, Name = "Sam", Position = 10 },
            new SaveData{HighScore = 5000, Name = "Sam", Position = 10 }
        };

    
        for(int i = 0; i < HighScores.Count; i++)
        {
            for(int j = i + 1; j < HighScores.Count; j++ )
            {
                if (HighScores[j].HighScore > HighScores[i].HighScore)
                {
                    SaveData tmp = HighScores[i];
                    HighScores[i] = HighScores[j];
                    HighScores[j] = tmp;
                }
            }
        }
    
    
    
    
    
    
    
    
    }


    void Start()
    {

  
 

        for (int i = 0; i <  10; i++)
        {
            Transform Scores = Instantiate(Template, Container);

            Scores.localPosition = new Vector2(0, -BaseHeight * i);

            if (Scores.Find("PositionTemplate").GetComponent<Text>() != null)
            {
                Scores.Find("PositionTemplate").GetComponent<Text>().text = ((10 - i).ToString() + ".");

                
            }

            


        }


        Destroy(TemplateGO);
         

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
