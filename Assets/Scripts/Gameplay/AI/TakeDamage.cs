﻿using UnityEngine;

public class TakeDamage : MonoBehaviour
{
    [Header("Controls")]
    public float HealthPoints;

    private float Clamp;
    private Material Death;



    private void Start()
    {
      
    }


    public void DamageTaken(float R_Damage)
    {
       

        float currentHealth = HealthPoints;

        HealthPoints -= R_Damage;

         Clamp = 0.75f - (HealthPoints / currentHealth);
         Clamp = Mathf.Clamp(Clamp, 0, 0.75f);

        

        


        if (HealthPoints <= 0)
        {
            DeathAnimation();
        }
    }


    private void Update()
    {
       // Death.SetFloat("Vector1_70A18B63", Clamp);
    }

    void DeathAnimation()
    {
        if(gameObject.GetComponentInParent<Patrol>() != null && gameObject.GetComponent<Patrol>() != null)
        {
            if (gameObject.tag == "Drone")
            {
                gameObject.GetComponentInParent<Patrol>().enabled = false;
            }
           
            else
            {
                gameObject.GetComponent<Patrol>().enabled = false;
            }
            
        }
        else if (gameObject.GetComponent<Turret>() != null)
        {
             
            gameObject.GetComponent<Turret>().Reloading = true;
            FindObjectOfType<Turret>().LightCheck();

        }


        Destroy(transform.root.gameObject, 4);
    }

}
