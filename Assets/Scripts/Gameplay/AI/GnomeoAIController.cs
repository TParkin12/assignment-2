﻿using System.Collections;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

// We inherit the AICharacter Control object from UnityStandardAssets.Characters.ThirdPerson
public class GnomeoAIController : AICharacterControl {

    public float Health;
    public float Damage;


    private void Awake()
    {
        Health = 100;

        Damage = 10;
         
    }

    // overwrite the update function in AICharacterControl.cs
    void Update() {

        if (target != null && Health != 0)
        {
            agent.SetDestination(target.position);

            // use the values to move the character
            character.Move(agent.desiredVelocity, false, false);
        }
        else
        {
            // We still need to call the character's move function, but we send zeroed input as the move param.
            character.Move(Vector3.zero, false, false);
        }

        // check if the agent is near to the player
        if (Vector3.Distance(target.position, transform.position) < 1.5f)
        {
            Kamikaze();
        }

        
        
        




    }




    public float HealthCheck(float DamageRecieved)
    {
        Health -= DamageRecieved;

        FindObjectOfType<HudManagment>().AddToScore(10);

        if (Health <= 0)
        {
            Death();
        }

        return (DamageRecieved);

    }

    private void Death()
    {
        Destroy(gameObject);
        FindObjectOfType<HudManagment>().AddToScore(100);
    }

    private void Kamikaze()
    {
        FindObjectOfType<PlayerStats>().DamageRecieved(Damage);

        Destroy(gameObject );

        
    }

}
