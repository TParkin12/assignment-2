﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

    [SerializeField] private float minTime;
    [SerializeField] private float maxTime;
    [SerializeField] public int Max_Spawn;

    [SerializeField] private Transform player;
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private Transform[] spawnPoints;
    
    private float timeDelta = 0;
    private float TempMin;
    public bool StartSpawning;

    void Start()
    {
        Debug.Assert(enemyPrefab != false, "Enemy prefab unassigned");
        timeDelta = Random.Range(minTime, maxTime);

        TempMin = 0;
    }

    void Update()
    {
        // Whilst timeDelta is above zero, we minus the time between the last update calls then check timeDelta again
        // if it is below zero we know the correct time has passed and we spawn an enemy from one of four random spawn
        // points. We then reset timeDelta to a random value.
        if (timeDelta > 0 )
        {
            timeDelta -= Time.deltaTime;
            if(timeDelta <= 0 )
            {
                if (StartSpawning && TempMin < Max_Spawn)
                {
                    int randomPoint = Random.Range(0, spawnPoints.Length);
                    Vector3 pos = spawnPoints[randomPoint].position;
                    GameObject enemy = Spawn(pos);
                    if(enemy.GetComponent<GnomeoAIController>() != null)
                    {
                        enemy.GetComponent<GnomeoAIController>().SetTarget(player);
                    }
                    else if(enemy.GetComponentInChildren<Turret>() != null)
                    {
                        enemy.GetComponentInChildren<LookTowards>().Target = player;
                    }
                   


                    TempMin++;
                }
                

                Debug.Log(TempMin);
                timeDelta = Random.Range(minTime, maxTime);

            }
            else if (StartSpawning == false || TempMin > Max_Spawn + 1)
            {
                TempMin = 0;
            }
        }

        
    }

    public GameObject Spawn(Vector3 pos)
    {
        return (GameObject)Instantiate(enemyPrefab, pos, Quaternion.identity);
    }


    public void SpawnIncrease(int Increase)
    {
        Max_Spawn += Increase;
    }



}
