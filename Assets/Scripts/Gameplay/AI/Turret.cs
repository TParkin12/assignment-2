﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private Transform Target;
    [SerializeField] private GameObject Barrel1;
    [SerializeField] private GameObject Barrel2;
    [Header("Controls")]
    public float T_RotationSpeed;
    public bool Reloading;
    public float BarrelRotationSpeed;
    public float ReloadTime;
    public float Range;
    public float Damage;
    public float FireRate;

    [Header("Local Variables")]
    [SerializeField]private float RotationSpeed;
    private bool canFire;

    private void Start()
    {
        Reloading = false;

        RotationSpeed = BarrelRotationSpeed;

 
    }
 

    private void OnTriggerStay(Collider other)
    {
        Fire();
        Rotation();
    }
    void Rotation()
    {

        Vector3 Direction;
        Quaternion rotation;
        Direction = Target.position - transform.position;
        rotation = Quaternion.LookRotation(Direction);

        if (Reloading != true)
        {
            Barrel1.GetComponentInChildren<Light>().color = Color.red;
            Barrel2.GetComponentInChildren<Light>().color = Color.red;

            rotation.x = Mathf.Clamp(rotation.x, -60, 60);
            transform.rotation = Quaternion.Lerp(transform.rotation, rotation, T_RotationSpeed * Time.deltaTime);


            RotationSpeed += RotationSpeed * Time.deltaTime;

            RotationSpeed = Mathf.Clamp(RotationSpeed, 0, 10000);

            if (RotationSpeed == 10000)
            {
                StartCoroutine(Reload());
            }

            Barrel1.transform.localRotation = Quaternion.Euler(Barrel1.transform.localRotation.x, RotationSpeed, Barrel1.transform.localRotation.z);

            Barrel2.transform.localRotation = Quaternion.Euler(Barrel2.transform.localRotation.x, RotationSpeed, Barrel2.transform.localRotation.z);
        }

        else
        {
            LightCheck();
        }

    }

    void Fire()
    {
        RaycastHit Hit;

        StartCoroutine(ROF());


        if (Physics.Raycast(transform.position, transform.forward, out Hit, Range) && Reloading == false && canFire)
        {
            Debug.DrawRay(transform.position, transform.forward * Range, Color.red);
            
            if(Hit.transform.GetComponent<PlayerStats>() != null)
            {
                Hit.transform.GetComponent<PlayerStats>().DamageRecieved(Damage);
            }

            
           
        }

        
    }

    public void LightCheck()
    {
        //if (gameObject.GetComponent<TakeDamage>() != null && gameObject.GetComponent<TakeDamage>().HealthPoints > 0)
        //{
        //    Barrel1.GetComponentInChildren<Light>().color = Color.blue;
        //    Barrel2.GetComponentInChildren<Light>().color = Color.blue;
        //}
        //else
        //{
        //    Barrel1.GetComponentInChildren<Light>().color = Color.green;
        //    Barrel2.GetComponentInChildren<Light>().color = Color.green;
        //}

      
        
    }


    IEnumerator ROF()
    {
        canFire = true;

        yield return new WaitForSeconds(FireRate);

        canFire = false;
    }

    IEnumerator Reload()
    {
        Reloading = true;

        yield return new WaitForSeconds(ReloadTime);

        RotationSpeed = BarrelRotationSpeed;

        Reloading = false;
    }

}
