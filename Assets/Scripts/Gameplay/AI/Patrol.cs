﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;



[HideInInspector]public enum EnemyState{

    PATROLING,
    CHASING
}



public class Patrol : MonoBehaviour
{

    [Header("References")]
    [SerializeField] private NavMeshAgent NavAgent;
    [SerializeField] private Transform Player;
    [SerializeField] private Transform[] patrolTargets;
    
    [Header("Controls")]
    public float AttackRange;
    public float TrailDistance;
    [Range(0f, 1f)]
    public float AttackChance;
    [Range(0f, 1f)]
    public float Accuracy;
    public float Damage;


   [Header("Local Variables")]
   [HideInInspector]public EnemyState State;
   private int currentTarget = 0;
   private TakeDamage Health;
   private float CurrentHealth;

    void Start()
    {

        NavAgent = GetComponent<NavMeshAgent>();
        NavAgent.SetDestination(patrolTargets[currentTarget].position);

        if(transform.parent.gameObject.name == "Flying AI") //Find way to make sure that there can be more than one Flying AI
        {
            Health = gameObject.GetComponentInChildren<TakeDamage>();
        }

        else
        {
            Health = gameObject.GetComponent<TakeDamage>();
        }
        
        CurrentHealth = Health.HealthPoints;
    }

    // Update is called once per frame
    void Update()
    {



        Player = GameObject.FindGameObjectWithTag("Player").transform;


        float Distance = Vector3.Distance(Player.transform.position, transform.position);
        bool fire;
        bool Follow = (Distance < TrailDistance);





        RaycastHit hit;
        var rayDirection = Player.position - transform.position;

        float dotProduct = Vector3.Dot(transform.forward, rayDirection);

        if (Physics.Raycast(transform.position, rayDirection, out hit) && Follow == true || (CurrentHealth > Health.HealthPoints))
        {
            CurrentHealth = Health.HealthPoints;

            if (hit.transform == Player)
            {
                State = EnemyState.CHASING; // enemy can see the player!
            }
            else
            {
                State = EnemyState.PATROLING;
                NavAgent.SetDestination(patrolTargets[currentTarget].position);

                // there is something obstructing the view
            }
        }


        switch (State)
        {
            case EnemyState.PATROLING:
                if (NavAgent.remainingDistance <= 0)
                {
                    currentTarget++;
                }

                if (currentTarget >= patrolTargets.Length)
                {
                    currentTarget = 0 ; //change so that the enemies go backwards from the last point to first plz
                }

                NavAgent.SetDestination(patrolTargets[currentTarget].position);
                break;




            case EnemyState.CHASING:
                NavAgent.SetDestination(Player.position);

                float random = Random.Range(0.0f, 1.0f);
                if (random > (1.0f - AttackChance) && Distance < AttackChance )
                {
                    fire = true;
                  
                }

                break;





        }






        

    }
}
