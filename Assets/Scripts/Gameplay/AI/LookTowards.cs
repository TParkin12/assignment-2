﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookTowards : MonoBehaviour
{
    [Header("References")]
    public Transform Target;
    [Header("Controls")]
    public float speed;
    private void Update()
    {
          
       
            Vector3 Direction = Target.position - transform.position;
            Quaternion rotation = Quaternion.LookRotation(Direction);
            transform.rotation = Quaternion.Lerp(transform.rotation, rotation, speed * Time.deltaTime);
        
        
    }
}
