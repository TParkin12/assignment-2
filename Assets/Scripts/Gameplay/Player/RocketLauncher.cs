﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketLauncher : MonoBehaviour
{

    [Header("References")]
    public GameObject RPG;

    [Header("Controls")]
    public float RocketSpeed;
    public float Ammo_Stock;
    public float MagazineSize;
    public float ReloadTime;
    public float FireRate;
    public float Ex_Damage;

    [Header("LocalVariables")]
    private Transform M_Pos;
    [SerializeField]private float CurrentAmmo;
    private bool IsReloading;
    private bool CanFire = true;

    // Start is called before the first frame update
    void Start()
    {
        M_Pos = transform;
        CurrentAmmo = MagazineSize;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0) && (CurrentAmmo != 0 && Ammo_Stock >= 0) && IsReloading == false && CanFire == true)
        {
           StartCoroutine(ShootRPG());
        }
        if(Input.GetKeyDown(KeyCode.R) || (CurrentAmmo == 0 && Ammo_Stock > 0 ))
        {
            StartCoroutine(ReloadRPG());
        }
    }


    IEnumerator ShootRPG()
    {
        CanFire = false;

        FindObjectOfType<AudioManager>().Play("Rocket");

        CurrentAmmo--;

        GameObject Projectile = Instantiate(RPG, M_Pos.transform.TransformPoint(-2f, 0, 0), M_Pos.rotation);
        Projectile.GetComponent<Rigidbody>().AddForce(-M_Pos.right * RocketSpeed, ForceMode.Impulse);
        Destroy(Projectile, 4);

        yield return new WaitForSeconds(FireRate);

        CanFire = true;

    }

    IEnumerator ReloadRPG()
    {
       
        IsReloading = true;

        FindObjectOfType<AudioManager>().Play("ShotgunReload");

        if (CurrentAmmo == 0 && Ammo_Stock > MagazineSize)
        {
            Ammo_Stock -= MagazineSize;

            CurrentAmmo = MagazineSize;
        }
        else if(CurrentAmmo != 0 && CurrentAmmo != MagazineSize)
        {
            Ammo_Stock -= ( MagazineSize - CurrentAmmo);

            CurrentAmmo = MagazineSize;
        }
        else if (Ammo_Stock < MagazineSize)
        {

            CurrentAmmo = Ammo_Stock;

            Ammo_Stock = 0;
        }

        yield return new WaitForSeconds(ReloadTime);


        IsReloading = false;
    }

}
