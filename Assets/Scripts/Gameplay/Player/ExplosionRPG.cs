﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionRPG : MonoBehaviour
{

    [Header("References")]
    public GameObject Ex_VFX;

    [Header("Controls")]
    public float Ex_Radius;
    public float Ex_Force;  
    public float Up_Modifer;

    [Header("LocalVariables")]
    private Collider[] Col_Hit;
   

    public void OnCollisionEnter(Collision collision)
    {
        ExplosionLocation(collision.contacts[0].point);
    }
 


    void ExplosionLocation(Vector3 Location)
    {
        Col_Hit = Physics.OverlapSphere(Location, Ex_Radius);



         

        GameObject ExplosionVFX = Instantiate(Ex_VFX, transform.TransformPoint(0,0,0), transform.rotation);

        Destroy(ExplosionVFX, 2.4f);


        foreach (Collider hitcol in Col_Hit)
        {
            if(hitcol.GetComponent<GnomeoAIController>() != null)
            {
                hitcol.GetComponent<GnomeoAIController>().HealthCheck(GameObject.Find("RocketLauncher").GetComponent<RocketLauncher>().Ex_Damage);
            }

            //if(hitcol.GetComponent<PlayerStats>() != null)
            //{
            //    hitcol.GetComponent<PlayerStats>().DamageTaken(Ex_Damage);
            //}

            if (hitcol.GetComponent<Rigidbody>() != null && !hitcol.CompareTag("Player"))
            {
                hitcol.GetComponent<Rigidbody>().isKinematic = false;
                hitcol.GetComponent<Rigidbody>().AddExplosionForce(Ex_Force, Location, Ex_Radius, Up_Modifer, ForceMode.Impulse);

               

               

                

            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, Ex_Radius);
    }

     
}
