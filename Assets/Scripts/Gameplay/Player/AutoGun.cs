﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
public class AutoGun : MonoBehaviour
{

    [SerializeField] private GameObject buckshotPrefab;

    [SerializeField] private Transform barrel;
    [SerializeField] private GameObject collisionCone;

    [SerializeField] private Vector3 sightVector;
    [Range(0, 1)]
    [SerializeField] private float range;

    //private Animator anim;

    public int MaxAmmo;
    private int CurrentAmmo;
    public float ReloadTime = 0f;
    public float ShotTime = 0f;
    public float HitDamage;
    private bool isReload = false;
    private bool isFireing = false;
    //public Animator A_Reload;
    //public Animator A_Fireing;




    void Start()
    {
        //anim = GetComponent<Animator>();
        if (CurrentAmmo == 0)
        {
            CurrentAmmo = MaxAmmo;
        }

        Cursor.lockState = CursorLockMode.Locked;

        collisionCone.transform.localScale = new Vector3(range, collisionCone.transform.localScale.y, collisionCone.transform.localScale.z);
    }


    private void OnEnable()
    {

       // A_Reload.SetBool("Reloading", false);

       // A_Fireing.SetBool("isFireing", false);

    }


    void Update()
    {
        if (isReload == true)
        {
            return;
        }
        if (CurrentAmmo == 0 || Input.GetKeyDown(KeyCode.R))
        {

            StartCoroutine(Reload());


            return; //code will stop here, and not continue

        }

        if (Input.GetButton("Fire1"))
        {

            if (isFireing == true)
            {

                return;
            }
            if (isFireing == false)
            {
                Fire();
                StartCoroutine(Fireing());
                return; //code will stop here, and not continue

            }
        }


    }

    IEnumerator Fireing()
    {
        isFireing = true;

        //A_Fireing.SetBool("isFireing", true);

        yield return new WaitForSeconds(ShotTime + 0.2f);

       // A_Fireing.SetBool("isFireing", false);

        isFireing = false;
    }



    IEnumerator Reload()
    {
        isReload = true;
        CurrentAmmo = MaxAmmo;

        FindObjectOfType<AudioManager>().Play("ShotgunReload");
        FindObjectOfType<AudioManager>().Stop("ShotgunFire");

        // A_Reload.SetBool("Reloading", true);
        //anim.GetCurrentAnimatorClipInfo(0).Length /
         yield return new WaitForSeconds(ReloadTime);

      //  A_Reload.SetBool("Reloading", false);

        isReload = false;

    }

    private void Fire()
    {
        CurrentAmmo--;


        // create the shooting particle effect
        GameObject emitter = (GameObject)Instantiate(buckshotPrefab);
        emitter.transform.SetParent(barrel, false);
        emitter.transform.localPosition = Vector3.zero;
        emitter.transform.localEulerAngles = Vector3.zero;
        Destroy(emitter, 2);

        FindObjectOfType<AudioManager>().Play("ShotgunFire");

        // Create the collision cone to see if th enemies are inside the collider
        GameObject cone = (GameObject)Instantiate(collisionCone);
        cone.transform.SetParent(barrel, false);
        cone.transform.localPosition = Vector3.zero;
        cone.transform.localEulerAngles = Vector3.zero;
        Destroy(cone, 0.1f);




    }
}

