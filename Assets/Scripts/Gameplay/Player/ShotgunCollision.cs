﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
public class ShotgunCollision : MonoBehaviour {

    [SerializeField] private GameObject ragdollPrefab;
    [SerializeField] private float force;
    [SerializeField] private GameObject Shotgun;
    [SerializeField] private GameObject Auto;



    public float Score;
    private float Damage;

    void Awake()
    {
        Damage = Auto.GetComponent<AutoGun>().HitDamage;

    }
 
    void OnTriggerEnter(Collider other)
    {
        // Replace the enemy with a ragdoll and add force to replicate knockback
        if(other.CompareTag("Enemy"))
        {
           

             
            
            other.GetComponent<GnomeoAIController>().HealthCheck(Damage);         

           
            if(other.GetComponent<GnomeoAIController>() != null && other.GetComponent<GnomeoAIController>().Health <= 0)
            {
                GameObject rg = (GameObject)Instantiate(ragdollPrefab, other.transform.position, other.transform.rotation);


                // move the rigidbody up so it does not intersect with the ground collider on spawn (causes wierd behaviour otherwise)
                rg.transform.position = new Vector3(other.transform.position.x, 0.2f, other.transform.position.z);

                // Apply an explosive force to the ragdoll, propelling them upwards and away (because it looks better)
                float randomForce = Random.Range(force * 0.7f, force * 1.3f);
                rg.GetComponentInChildren<Rigidbody>().AddForce(-transform.forward * randomForce / 2, ForceMode.Impulse);
                rg.GetComponentInChildren<Rigidbody>().AddForce(transform.up * randomForce, ForceMode.Impulse);

                

                Destroy(rg, 5);
            }
            
        }
       
        
    }

     
    


}
