﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public float HealthPoints;

    public bool doubleDamage = false;


    public void DamageRecieved(float Damage)
    {

        if (doubleDamage)
        {
            HealthPoints -= Damage * 2;
        }
        else
        {
            HealthPoints -= Damage;
        }
       

        if(HealthPoints <= 0)
        {
            HealthPoints = 0;
            FindObjectOfType<DeathAnimation>().IsKilled = true;
        }
    }
}
