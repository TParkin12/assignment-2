﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathAnimation : MonoBehaviour
{

    public bool IsKilled;

    [SerializeField] private Animator anim;  


    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if(IsKilled)
        {
            anim.SetTrigger("DeathAnimation");

            StartCoroutine(DeathDelay());

           
        }



    }


    IEnumerator DeathDelay()
    {
        FindObjectOfType<GameManager>().EnableInput(false);

     

        yield return new WaitForSeconds(3);

        GameManager.Instance.GameOver();


    }
}
